﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson22_HW
{
    class Camp
    {
        private readonly int ID;
        public int Latitude { get; private set; }
        public int Longitude { get; private set; }
        public int NumberOfPeople { get; private set; }
        public int NumberOfTents { get; private set; }
        public int NumberOfFlashLights { get; private set; }
        private static int lastCampId = 0;

        public Camp(int latitude, int longitude, int numberOfPeople, int numberOfTents, int numberOfFlashLights)
        {
            Latitude = latitude;
            Longitude = longitude;
            NumberOfPeople = numberOfPeople;
            NumberOfTents = numberOfTents;
            NumberOfFlashLights = numberOfFlashLights;

            lastCampId++;
            this.ID = lastCampId;
        }

        public override string ToString()
        {
            return $"ID: {this.ID}  Latitude: {this.Latitude}  Longitude: {this.Longitude}  People Number: {this.NumberOfPeople}  Tents Number: {this.NumberOfTents}  Flashlights Number: {this.NumberOfFlashLights}";
        }

        public static bool operator ==(Camp camp1, Camp camp2)
        {
            if (ReferenceEquals(camp1, null)&& ReferenceEquals(camp2,null))
            {
                return true;
            }
            if (ReferenceEquals(camp1, null) || ReferenceEquals(camp2, null))
            {
                return false;
            }
            return camp1.ID == camp2.ID;
        }

        public static bool operator !=(Camp camp1, Camp camp2)
        {
            return !(camp1 == camp2);
        }

        public static bool operator >(Camp camp1, Camp camp2)
        {
            return camp1.NumberOfPeople > camp2.NumberOfPeople;
        }

        public static bool operator <(Camp camp1, Camp camp2)
        {
            return camp1.NumberOfPeople < camp2.NumberOfPeople;
        }

        public bool Equales(Object o)
        {
            Camp camp = o as Camp;
            return this == camp;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }

        public static Camp operator +(Camp camp1, Camp camp2)
        {
            return new Camp ((camp1.Latitude + camp2.Latitude)/2, (camp1.Longitude + camp2.Longitude)/2,
                               camp1.NumberOfPeople + camp2.NumberOfPeople, camp1.NumberOfTents + camp2.NumberOfTents,
                               camp1.NumberOfFlashLights + camp2.NumberOfFlashLights);
        }

    }
}
