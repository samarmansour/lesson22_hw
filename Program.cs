﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson22_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp camp1 = new Camp(15,20,10,5,25);
            Camp camp2 = new Camp(50,80,30,20,100);

            if (camp1 > camp2)
            {
                Console.WriteLine("camp1 is bigger");
            }
            else
            {
                Console.WriteLine("camp2 is bigger");
            }

            Camp camp3 = camp1 + camp2;
            Console.WriteLine(camp3);

            Console.WriteLine("=============================");
            string path = @"..\..\campFile.xml";
            CampChallenge campChallenge = new CampChallenge(15, 20, 10, 5, 25);
            CampChallenge campChallenge2 = new CampChallenge(50, 80, 30, 20, 100);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CampChallenge));

            using (Stream file = new FileStream(path, FileMode.Create))
            {
                xmlSerializer.Serialize(file,campChallenge);
            }

            using (Stream file = new FileStream(path, FileMode.Create))
            {
                xmlSerializer.Serialize(file, campChallenge2);
            }

            CampChallenge readFirstCamp = null;
            CampChallenge readSecondCamp = null;

            using (Stream file = new FileStream(path, FileMode.Open))
            {
                readFirstCamp = xmlSerializer.Deserialize(file) as CampChallenge;
            }

            using (Stream file = new FileStream(path, FileMode.Open))
            {
                readSecondCamp = xmlSerializer.Deserialize(file) as CampChallenge;
            }

            if (readFirstCamp == readSecondCamp)
            {
                Console.WriteLine("Equals");
            }
            else
            {
                Console.WriteLine("Not Equal");
            }
            Console.WriteLine(readFirstCamp.GetHashCode() == readSecondCamp.GetHashCode());



        }
    }
}
